# template.sh - edit this file
function usage
{
  echo "usage: $0 arguments ..."
  if [ $# -eq 1 ]
  then echo "ERROR: $1"
  fi
}

# Your script starts after this line.

#Check For arguments
if [ $# -eq 0 ]
    then usage
else
  echo "Liam Bryant"
  date +"%a %b %d %T %Z %Y"
  echo ""

  #For loop Through arguments
  for var in $@
  do

    #Check for "Various string"
    case $var in

      "TestError") 
        usage "TestError found"
        ;;
      "now")
	      echo "It is now $(date +"%I:%M:%S %p")"
        ;;
      *)
        usage "Do not know what to do with $var"
	;;
    esac

    #Argument Seperator 
    echo "*****"
  done

fi
